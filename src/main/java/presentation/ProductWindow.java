package presentation;

import bll.ClientBLL;
import bll.ProductBLL;
import bll.WarehouseBLL;
import model.Product;
import model.Warehouse;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class ProductWindow implements ActionListener {   private JFrame frame = new JFrame("Product Processing");

    private JPanel info = new JPanel();
    private JPanel actions = new JPanel();
    private JPanel tablePanel = new JPanel();

    private JTable table;
    private DefaultTableModel model;




    private JLabel idLabel = new JLabel("Product ID:");
    private JLabel nameLabel = new JLabel("Name:");
    private JLabel productPriceLabel = new JLabel("Price:");
    private JLabel quantityLabel = new JLabel ("Quantity:");

    private JTextField idText = new JTextField();
    private JTextField nameText = new JTextField();
    private JTextField productPriceText = new JTextField();
    private JTextField quantityText = new JTextField();

    private int productID;
    private String productName;
    private int productPrice;
    private int quantity;

    private JButton addProduct = new JButton("Add Product");
    private JButton updateProduct = new JButton("Update Product");
    private JButton deleteProduct = new JButton("Delete Product");
    private JButton view = new JButton("View Table");

    private JTable productsTable;
    // ArrayList<Product> products = ProductDAO.findAllProducts();

    private JScrollPane scrollPane;

    public  ProductWindow() {

        frame.setSize(700, 700);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setLayout(new BorderLayout());
        frame.setVisible(true);

        info.setLayout(new GridLayout(5, 2));
        info.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
        info.setVisible(true);

        info.add(idLabel);
        info.add(idText);
        info.add(nameLabel);
        info.add(nameText);
        info.add(productPriceLabel);
        info.add(productPriceText);
        info.add(quantityLabel);
        info.add(quantityText);

        idText.addActionListener(this);
        nameText.addActionListener(this);
        productPriceText.addActionListener(this);

        actions.setLayout(new FlowLayout());

        actions.add(addProduct);
        actions.add(updateProduct);
        actions.add(deleteProduct);
        actions.add(view);

        addProduct.setActionCommand("add");
        updateProduct.setActionCommand("update");
        deleteProduct.setActionCommand("delete");
        view.setActionCommand("view");

        addProduct.setBackground(Color.CYAN);
        updateProduct.setBackground(Color.CYAN);
        deleteProduct.setBackground(Color.CYAN);
        view.setBackground(Color.CYAN);

        addProduct.addActionListener(this);
        updateProduct.addActionListener(this);
        deleteProduct.addActionListener(this);
        view.addActionListener(this);

        tablePanel.setLayout(new FlowLayout());

        model = new DefaultTableModel();

        this.table = new JTable(model);
        JScrollPane scrollPane = new JScrollPane(table);
        model.addColumn("Product ID");
        model.addColumn("Name");
        model.addColumn("Price");
        tablePanel.add(scrollPane, BorderLayout.CENTER);

        frame.add(info, BorderLayout.NORTH);
        frame.add(actions, BorderLayout.CENTER);
        frame.add(tablePanel, BorderLayout.SOUTH);
    }

    public  void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("add")) {
            productID = Integer.parseInt(idText.getText());
            productName = nameText.getText();
            productPrice = Integer.parseInt(productPriceText.getText());
            quantity = Integer.parseInt(quantityText.getText());

            Product c = new Product(productID, productName,  productPrice);
            int id = ProductBLL.addProduct(c);
            WarehouseBLL.addWarehouse(new Warehouse(0,productID,quantity));

        }
        else if (e.getActionCommand().equals("update")) {
            productID = Integer.parseInt(idText.getText());
            productName = nameText.getText();
            productPrice = Integer.parseInt(productPriceText.getText());
            quantity = Integer.parseInt(quantityText.getText());

            Product c = new Product(productID, productName,  productPrice);
            ProductBLL.updateProduct(c);
            WarehouseBLL.updateWarehouse(new Warehouse(0,productID,quantity));
        }
        else if (e.getActionCommand().equals("delete")) {
            productID = Integer.parseInt(idText.getText());

            Product c = new Product(productID, productName,  productPrice);
            ProductBLL.deleteProduct(c);
        }
        else if (e.getActionCommand().equals("view")) {
            //this.refresh(ProductBLL.getProducts());
            this.table.setModel(ClientBLL.createJTable(ProductBLL.getProducts()).getModel());

        }
    }

    public void addRow(int productId, String productName, int productPrice){
        model.addRow(new Object[] {productId, productName, productPrice});
    }

    public void addProduct(Product product){
        this.addRow(product.getProductID(), product.getProductName(), product.getProductPrice());
    }


    public void refresh(List<Product> products){

        model.setRowCount(0);
        for(Product product : products){
            addProduct(product);
        }
    }





}
