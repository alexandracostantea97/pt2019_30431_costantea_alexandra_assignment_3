package presentation;

import bll.ClientBLL;
import bll.OrderBLL;
import bll.ProductBLL;
import model.Order;
import model.Product;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Date;

public class OrderWindow implements ActionListener {

    private JFrame frame = new JFrame("Order Processing");

    private JPanel info = new JPanel();
    private JPanel actions = new JPanel();
    private JPanel tablePanel = new JPanel();

    private JTable table;
    private DefaultTableModel model;




    private JLabel productIDLabel = new JLabel("Product ID:");
    private JLabel clientIDLabel = new JLabel("Client ID:");
    private JLabel quantityLabel = new JLabel("Quantity:");
    private JLabel orderIDLabel = new JLabel("Order ID:");

    private JTextField prodIDText = new JTextField();
    private JTextField clientIDText = new JTextField();
    private JTextField quantityText = new JTextField();
    private JTextField orderIDText = new JTextField();

    private int orderID;
    private int productID;
    private int clientID;
    private int quantity;

    private JButton addOrder = new JButton("Add Order");
    private JButton updateOrder = new JButton("Update Order");
    private JButton deleteOrder = new JButton("Delete Order");
    private JButton view = new JButton("View Table");

    private JTable ordersTable;
    // ArrayList<Order> orders = OrderDAO.findAllOrders();

    private JScrollPane scrollPane;

    public  OrderWindow() {

        frame.setSize(700, 700);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setLayout(new BorderLayout());
        frame.setVisible(true);

        info.setLayout(new GridLayout(5, 2));
        info.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
        info.setVisible(true);


        info.add(orderIDLabel);
        info.add(orderIDText);
        info.add(productIDLabel);
        info.add(prodIDText);
        info.add(clientIDLabel);
        info.add(clientIDText);
        info.add(quantityLabel);
        info.add(quantityText);

        prodIDText.addActionListener(this);
        clientIDText.addActionListener(this);
        quantityText.addActionListener(this);
        orderIDText.addActionListener(this);


        actions.setLayout(new FlowLayout());

        actions.add(addOrder);
        actions.add(updateOrder);
        actions.add(deleteOrder);
        actions.add(view);

        addOrder.setActionCommand("add");
        updateOrder.setActionCommand("update");
        deleteOrder.setActionCommand("delete");
        view.setActionCommand("view");

        addOrder.setBackground(Color.CYAN);
        updateOrder.setBackground(Color.CYAN);
        deleteOrder.setBackground(Color.CYAN);
        view.setBackground(Color.CYAN);

        addOrder.addActionListener(this);
        updateOrder.addActionListener(this);
        deleteOrder.addActionListener(this);
        view.addActionListener(this);

        tablePanel.setLayout(new FlowLayout());

        model = new DefaultTableModel();

        this.table = new JTable(model);
        JScrollPane scrollPane = new JScrollPane(table);
        model.addColumn("Order ID");
        model.addColumn("Name");
        model.addColumn("Address");
        tablePanel.add(scrollPane, BorderLayout.CENTER);

        frame.add(info, BorderLayout.NORTH);
        frame.add(actions, BorderLayout.CENTER);
        frame.add(tablePanel,BorderLayout.SOUTH);
    }

    public  void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("add")) {
            productID = Integer.parseInt(prodIDText.getText());
            clientID = Integer.parseInt(clientIDText.getText());
            quantity = Integer.parseInt(quantityText.getText());
            orderID = Integer.parseInt(orderIDText.getText());

            Order c = new Order(orderID,productID, clientID,  quantity);
            int id = OrderBLL.addOrder(c);
            if(id == 0) {
                JOptionPane.showMessageDialog(null,"Under stock!");
            }

            else
                generateBill(c);

        }
        else if (e.getActionCommand().equals("update")) {
            productID = Integer.parseInt(prodIDText.getText());
            clientID = Integer.parseInt(clientIDText.getText());
            quantity = Integer.parseInt(quantityText.getText());
            orderID = Integer.parseInt(orderIDText.getText());

            Order c = new Order(orderID, productID, clientID,  quantity);
            OrderBLL.updateOrder(c);
        }
        else if (e.getActionCommand().equals("delete")) {
            orderID = Integer.parseInt(orderIDText.getText());

            Order c = new Order(orderID, productID, clientID,  quantity);
            OrderBLL.deleteOrder(c);
        }
        else if (e.getActionCommand().equals("view")) {
            //this.refresh(OrderBLL.getOrders());
            this.table.setModel(ClientBLL.createJTable(OrderBLL.getOrders()).getModel());

        }
    }

    public void generateBill(Order order) {

        try{
            PrintWriter writer = new PrintWriter("bill_" + order.getOrderID() + ".txt", "UTF-8");
            writer.println("Order: " + order.getOrderID());
            writer.println("Items:");
            Product p = ProductBLL.getProduct(order.getProductID());
            writer.println("Product\t Price\t Quantity\t");
            writer.println(p.getProductName()+ " " + p.getProductPrice()+ " " + order.getQuantity());
            writer.println("Total: " + order.getQuantity() * p.getProductPrice());
            writer.println("\n" + new Date());
            writer.close();
        }
        catch(FileNotFoundException e){
            e.printStackTrace();
        }
        catch(UnsupportedEncodingException e){
            e.printStackTrace();
        }
    }
    
}
