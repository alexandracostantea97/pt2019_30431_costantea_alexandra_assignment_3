package presentation;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MainFrame implements ActionListener{

        private JFrame frame = new JFrame();
        private JPanel panel = new JPanel();

        private JButton ClientProcessing;
        private JButton ProductProcessing;
        private JButton OrderProcessing;

        public MainFrame() {
            frame.setTitle("Assignment 3");
            frame.setVisible(true);
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.setSize(1200, 300);
            frame.setLocationRelativeTo(null);

            ClientProcessing = new JButton("Client Processing");
            ProductProcessing = new JButton("Product Processing");
            OrderProcessing = new JButton("Order Processing");

            ClientProcessing.setActionCommand("process client");
            ProductProcessing.setActionCommand("process product");
            OrderProcessing.setActionCommand("process order");

            ClientProcessing.addActionListener(this);
            ProductProcessing.addActionListener(this);
            OrderProcessing.addActionListener(this);

            ClientProcessing.setBackground(SystemColor.PINK);
            ClientProcessing.setFont(new Font("Times New Roman", Font.ROMAN_BASELINE, 20));

            ProductProcessing.setBackground(Color.PINK);
            ProductProcessing.setFont(new Font("Times New Roman", Font.ROMAN_BASELINE, 20));

            OrderProcessing.setBackground(Color.PINK);
            OrderProcessing.setFont(new Font("Times New Roman", Font.ROMAN_BASELINE, 20));

            panel.setLayout(new GridLayout(1,3));
            panel.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
            panel.setVisible(true);

            panel.add(ClientProcessing);
            panel.add(ProductProcessing);
            panel.add(OrderProcessing);

            frame.add(panel);
        }

        public  void actionPerformed(ActionEvent e) {
            if (e.getActionCommand().equals("process client")) {
               // ClientProcessing c = new ClientProcessing();
                ClientWindow c= new ClientWindow();
            }
            else if (e.getActionCommand().equals("process product")) {
               // ProductProcessing p = new ProductProcessing();
                ProductWindow p= new ProductWindow();
            }
            else if (e.getActionCommand().equals("process order")) {
               // OrderProcessing o = new OrderProcessing();
                OrderWindow o= new OrderWindow();
            }
        }

    }

