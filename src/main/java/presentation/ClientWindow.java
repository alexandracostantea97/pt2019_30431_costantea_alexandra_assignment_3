package presentation;

import bll.ClientBLL;
import model.Client;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.List;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ClientWindow implements ActionListener{

    private JFrame frame = new JFrame("Client Processing");

    private JPanel info = new JPanel();
    private JPanel actions = new JPanel();
    private JPanel tablePanel = new JPanel();

    private JTable table;
    private DefaultTableModel model;




    private JLabel idLabel = new JLabel("Client ID:");
    private JLabel nameLabel = new JLabel("Name:");
    private JLabel addressLabel = new JLabel("Address:");

    private JTextField idText = new JTextField();
    private JTextField nameText = new JTextField();
    private JTextField addressText = new JTextField();

    private int C_ID;
    private String C_name;
    private String address;

    private JButton addClient = new JButton("Add Client");
    private JButton updateClient = new JButton("Update Client");
    private JButton deleteClient = new JButton("Delete Client");
    private JButton view = new JButton("View Table");

    private JTable clientsTable;
   // ArrayList<Client> clients = ClientDAO.findAllClients();

    private JScrollPane scrollPane;

    public  ClientWindow() {

        frame.setSize(700, 700);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setLayout(new BorderLayout());
        frame.setVisible(true);

        info.setLayout(new GridLayout(5, 2));
        info.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
        info.setVisible(true);

        info.add(idLabel);
        info.add(idText);
        info.add(nameLabel);
        info.add(nameText);
        info.add(addressLabel);
        info.add(addressText);

        idText.addActionListener(this);
        nameText.addActionListener(this);
        addressText.addActionListener(this);

        actions.setLayout(new FlowLayout());

        actions.add(addClient);
        actions.add(updateClient);
        actions.add(deleteClient);
        actions.add(view);

        addClient.setActionCommand("add");
        updateClient.setActionCommand("update");
        deleteClient.setActionCommand("delete");
        view.setActionCommand("view");

        addClient.setBackground(Color.CYAN);
        updateClient.setBackground(Color.CYAN);
        deleteClient.setBackground(Color.CYAN);
        view.setBackground(Color.CYAN);

        addClient.addActionListener(this);
        updateClient.addActionListener(this);
        deleteClient.addActionListener(this);
        view.addActionListener(this);

        tablePanel.setLayout(new FlowLayout());

        model = new DefaultTableModel();

        this.table = new JTable(model);
        JScrollPane scrollPane = new JScrollPane(table);
        model.addColumn("Client ID");
        model.addColumn("Name");
        model.addColumn("Address");
        tablePanel.add(scrollPane, BorderLayout.CENTER);

        frame.add(info, BorderLayout.NORTH);
        frame.add(actions, BorderLayout.CENTER);
        frame.add(tablePanel,BorderLayout.SOUTH);
    }

    public  void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("add")) {
            C_ID = Integer.parseInt(idText.getText());
            C_name = nameText.getText();
            address = addressText.getText();

            Client c = new Client(C_ID, C_name,  address);
            int id = ClientBLL.addClient(c);

        }
        else if (e.getActionCommand().equals("update")) {
            C_ID = Integer.parseInt(idText.getText());
            C_name = nameText.getText();
            address = addressText.getText();

            Client c = new Client(C_ID, C_name,  address);
            ClientBLL.updateClient(c);
        }
        else if (e.getActionCommand().equals("delete")) {
            C_ID = Integer.parseInt(idText.getText());

            Client c = new Client(C_ID, C_name,  address);
            ClientBLL.deleteClient(c);
        }
        else if (e.getActionCommand().equals("view")) {
            //this.refresh(ClientBLL.getClients());
            this.table.setModel(ClientBLL.createJTable(ClientBLL.getClients()).getModel());

        }
    }

    public void addRow(int clientId, String clientName, String clientAddress){
        model.addRow(new Object[] {clientId, clientName, clientAddress});
    }

    public void addClient(Client client){
        this.addRow(client.getClientID(), client.getClientName(), client.getClientAddress());
    }


    public void refresh(List<Client> clients){

        model.setRowCount(0);
        for(Client client : clients){
           addClient(client);
        }
    }


}