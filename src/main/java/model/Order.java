package model;

public class Order {

    private int orderID;
    private int productID;
    private int clientID;
    private int quantity;

    public Order(int orderID, int productID, int clientID, int quantity) {
        this.orderID = orderID;
        this.productID = productID;
        this.clientID = clientID;
        this.quantity = quantity;
    }

    public int getOrderID() {
        return orderID;
    }

    public void setOrderID(int orderID) {
        this.orderID = orderID;
    }

    public int getProductID() {
        return productID;
    }

    public void setProductID(int productID) {
        this.productID = productID;
    }

    public int getClientID() {
        return clientID;
    }

    public void setClientID(int clientID) {
        this.clientID = clientID;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
