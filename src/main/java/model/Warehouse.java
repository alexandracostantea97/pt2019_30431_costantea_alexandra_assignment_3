package model;

public class Warehouse {
    private int warehouseID;
    private int prodID;
    private int productStock;

    public Warehouse(int warehouseID, int prodID, int productStock) {
        this.warehouseID = warehouseID;
        this.prodID = prodID;
        this.productStock = productStock;
    }

    public int getWarehouseID() {
        return warehouseID;
    }

    public void setWarehouseID(int warehouseID) {
        this.warehouseID = warehouseID;
    }

    public int getProdID() {
        return prodID;
    }

    public void setProdID(int prodID) {
        this.prodID = prodID;
    }

    public int getProductStock() {
        return productStock;
    }

    public void setProductStock(int productStock) {
        this.productStock = productStock;
    }
}
