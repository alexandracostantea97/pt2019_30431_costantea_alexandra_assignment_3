package model;

public class Client {

    private int clientID;
    private String clientName;
    private String clientAddress;

    public int getClientID() {
        return clientID;
    }

    public void setClientID(int clientID) {
        this.clientID = clientID;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getClientAddress() {
        return clientAddress;
    }

    public void setClientAddress(String clientAddress) {
        this.clientAddress = clientAddress;
    }

    public Client(int clientID, String clientName, String clientAddress) {
        this.clientID = clientID;
        this.clientName = clientName;
        this.clientAddress = clientAddress;
    }

    public Client( String clientName, String clientAddress) {
        this.clientName = clientName;
        this.clientAddress = clientAddress;
    }
}
