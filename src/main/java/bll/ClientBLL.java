package bll;

import dao.ClientDAO;
import model.Client;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class ClientBLL {

    private static ArrayList<Client> clients = new ArrayList<Client>();

    public static int addClient(Client client) {
        return ClientDAO.insertClient(client);
    }

    public static void updateClient(Client client) {
        ClientDAO.updateClient(client);
    }

    public static void deleteClient(Client client) {
        ClientDAO.deleteClient(client);
    }


    public static List getClients() {
       return  ClientDAO.showClients();
    }

    public static <T> JTable createJTable(List<T> list){
        T object = list.get(0);
        Field fields[] = object.getClass().getDeclaredFields();
        DefaultTableModel model = new DefaultTableModel();

        Method methods[] = object.getClass().getDeclaredMethods();
        List<Method> orderedMethods = new ArrayList<Method>();

        for(Field field : fields){
            model.addColumn(field.getName());
            for(Method m : methods){
                if(m.getName().toLowerCase().contains("get" + field.getName().toLowerCase())) {
                    orderedMethods.add(m);
                    break;
                }
            }
        }

        for(T o : list){
            try {
                Vector<Object> objectList = new Vector<Object>();
                for(Method m : orderedMethods){
                    objectList.add(m.invoke(o));
                }
                model.addRow(objectList);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        }
        return new JTable(model);
    }
}
