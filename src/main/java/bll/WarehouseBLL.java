package bll;

import dao.WarehouseDAO;
import model.Product;
import model.Warehouse;

import java.util.ArrayList;
import java.util.List;

public class WarehouseBLL {

    private static ArrayList<Warehouse> warehouses = new ArrayList<Warehouse>();

    public static int addWarehouse(Warehouse warehouse) {
        return WarehouseDAO.insertWarehouse(warehouse);
    }

    public static void updateWarehouse(Warehouse warehouse) {
        WarehouseDAO.updateWarehouse(warehouse);
    }

    public static void deleteWarehouse(Warehouse warehouse) {
        WarehouseDAO.deleteWarehouse(warehouse);
    }

    public static List getWarehouses() {
        return  WarehouseDAO.showWarehouses();
    }

    public static Warehouse getWarehouse(Product product){
        List<Warehouse> warehouses = getWarehouses();
        for(Warehouse w : warehouses){
            if(product.getProductID() == w.getProdID()){
                return w;
            }
        }

        return null;
    }
    
}
