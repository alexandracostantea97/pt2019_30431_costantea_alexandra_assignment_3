package bll;

import dao.OrderDAO;
import model.Order;
import model.Product;
import model.Warehouse;

import java.util.ArrayList;
import java.util.List;

public class OrderBLL {
    private static ArrayList<Order> orders = new ArrayList<Order>();

    public static int addOrder(Order order) {
        Product p = new Product(order.getProductID(),null,0);
        Warehouse w = WarehouseBLL.getWarehouse(p);
        if(w.getProductStock() >= order.getQuantity()){
            w.setProductStock(w.getProductStock() - order.getQuantity());
            WarehouseBLL.updateWarehouse(w);
            return OrderDAO.insertOrder(order);
        }
        else
         return 0;
    }

    public static void updateOrder(Order order) {
        OrderDAO.updateOrder(order);
    }

    public static void deleteOrder(Order order) {
        OrderDAO.deleteOrder(order);
    }

    public static List getOrders() {
        return  OrderDAO.showOrders();
    }
    
}
