package bll;

import dao.ProductDAO;
import model.Product;

import java.util.ArrayList;
import java.util.List;

public class ProductBLL {
    private static ArrayList<Product> products = new ArrayList<Product>();

    public static int addProduct(Product product) {
        return ProductDAO.insertProduct(product);
    }

    public static void updateProduct(Product product) {
        ProductDAO.updateProduct(product);
    }

    public static void deleteProduct(Product product) {
        ProductDAO.deleteProduct(product);
    }

    public static List getProducts() {
        return  ProductDAO.showProducts();
    }

    public static Product getProduct(int ID){
        List<Product> products = getProducts();
        for(Product product : products){
            if(product.getProductID() == ID )
            {
                return product;
            }
        }
        return null;
    }
}
