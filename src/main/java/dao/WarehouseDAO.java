package dao;

import connection.ConnectionFactory;
import model.Warehouse;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class WarehouseDAO {

    private static String insertStatement = "INSERT INTO warehouse (prodID, productStock)"+" VALUES (?,?)";
    private static String updateStatement = "UPDATE warehouse SET prodID=?, productStock=? WHERE warehouseID=?";
    private static String deleteStatement = "DELETE FROM warehouse WHERE warehouseID=?";
    private static String findStatement = "SELECT * FROM warehouse WHERE warehouseID=?";
    private static String statement = "SELECT * FROM warehouse";

    public static int insertWarehouse(Warehouse warehouse) {
        Connection dbConn = ConnectionFactory.getConnection();
        PreparedStatement insertS = null;

        int insertedID = -1;

        try
        {
            insertS = dbConn.prepareStatement(insertStatement, Statement.RETURN_GENERATED_KEYS);

            insertS.setInt(1, warehouse.getProdID());
            insertS.setInt(2, warehouse.getProductStock());
            insertS.executeUpdate();

            ResultSet rs = insertS.getGeneratedKeys();
            if (rs.next()) {
                insertedID = rs.getInt(1);
            }
        }
        catch(SQLException e)
        {
            System.out.println("SQLException: " + e.getMessage());
        }

        return insertedID;
    }

    public static void updateWarehouse(Warehouse warehouse) {
        Connection dbConn = ConnectionFactory.getConnection();
        PreparedStatement updateS = null;

        int updatedID = warehouse.getWarehouseID();

        try {
            updateS = dbConn.prepareStatement(updateStatement);

            updateS.setInt(1, warehouse.getProdID());
            updateS.setInt(2, warehouse.getProductStock());
            updateS.setInt(3,warehouse.getWarehouseID());
            updateS.executeUpdate();

        } catch (SQLException e) {
            System.out.println("SQLException: " + e.getMessage());


        }


    }

    public static void deleteWarehouse(Warehouse warehouse) {
        Connection dbConn = ConnectionFactory.getConnection();
        PreparedStatement deleteS = null;

        int deletedID = warehouse.getWarehouseID();

        try {
            deleteS = dbConn.prepareStatement(deleteStatement);
            deleteS.setInt(1,warehouse.getWarehouseID());
            deleteS.executeUpdate();

        } catch (SQLException e) {
            System.out.println("SQLException: " + e.getMessage());
        }

    }

    public static List<Warehouse> showWarehouses(){
        Connection dbConn = ConnectionFactory.getConnection();
        PreparedStatement selectS = null;
        List<Warehouse> warehouses = new ArrayList<Warehouse>();
        ResultSet resultSet;

        try {
            selectS = dbConn.prepareStatement(statement);
            resultSet = selectS.executeQuery();
            while(resultSet.next()){
                int ID = resultSet.getInt("warehouseID");
                int prodID = resultSet.getInt("ProdID");
                int productStock = resultSet.getInt("productStock");
                Warehouse warehouse = new Warehouse(ID,prodID,productStock);
                warehouses.add(warehouse);
            }
        } catch (SQLException e) {
            System.out.println("SQLException: " + e.getMessage());
        }

        return warehouses;
    }



}
