package dao;

import connection.ConnectionFactory;
import model.Client;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ClientDAO {

    private static String insertStatement = "INSERT INTO client (clientName, clientAddress)"+" VALUES (?,?)";
    private static String updateStatement = "UPDATE client SET clientName=?, clientAddress=? WHERE clientID=?";
    private static String deleteStatement = "DELETE FROM client WHERE clientID=?";
    private static String findStatement = "SELECT * FROM client WHERE clientID=?";
    private static String statement = "SELECT * FROM client";

    public static int insertClient(Client client) {
        Connection dbConn = ConnectionFactory.getConnection();
        PreparedStatement insertS = null;

        int insertedID = -1;

        try
        {
            insertS = dbConn.prepareStatement(insertStatement, Statement.RETURN_GENERATED_KEYS);

            insertS.setString(1, client.getClientName());
            insertS.setString(2, client.getClientAddress());
            insertS.executeUpdate();

            ResultSet rs = insertS.getGeneratedKeys();
            if (rs.next()) {
                insertedID = rs.getInt(1);
            }
        }
        catch(SQLException e)
        {
            System.out.println("SQLException: " + e.getMessage());
        }

        return insertedID;
    }

    public static void updateClient(Client client) {
        Connection dbConn = ConnectionFactory.getConnection();
        PreparedStatement updateS = null;

        int updatedID = client.getClientID();

        try {
            updateS = dbConn.prepareStatement(updateStatement);

            updateS.setString(1, client.getClientName());
            updateS.setString(2, client.getClientAddress());
            updateS.setInt(3,client.getClientID());
            updateS.executeUpdate();

        } catch (SQLException e) {
            System.out.println("SQLException: " + e.getMessage());


        }


    }

    public static void deleteClient(Client client) {
        Connection dbConn = ConnectionFactory.getConnection();
        PreparedStatement deleteS = null;

        int deletedID = client.getClientID();

        try {
            deleteS = dbConn.prepareStatement(deleteStatement);
            deleteS.setInt(1,client.getClientID());
            deleteS.executeUpdate();

        } catch (SQLException e) {
            System.out.println("SQLException: " + e.getMessage());
        }

    }

    public static List<Client> showClients(){
        Connection dbConn = ConnectionFactory.getConnection();
        PreparedStatement selectS = null;
        List<Client> clients = new ArrayList<Client>();
        ResultSet resultSet;

        try {
            selectS = dbConn.prepareStatement(statement);
            resultSet = selectS.executeQuery();
            while(resultSet.next()){
                int ID = resultSet.getInt("clientID");
                String name = resultSet.getString("clientName");
                String address = resultSet.getString("clientAddress");
                Client client = new Client(ID,name,address);
                clients.add(client);
            }
        } catch (SQLException e) {
            System.out.println("SQLException: " + e.getMessage());
        }

        return clients;
    }

}
