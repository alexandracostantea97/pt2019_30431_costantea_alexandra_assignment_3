package dao;

import connection.ConnectionFactory;
import model.Product;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ProductDAO {

    private static String insertStatement = "INSERT INTO product (productName, productPrice)"+" VALUES (?,?)";
    private static String updateStatement = "UPDATE product SET productName=?, productPrice=? WHERE productID=?";
    private static String deleteStatement = "DELETE FROM product WHERE productID=?";
    private static String findStatement = "SELECT * FROM product WHERE productID=?";
    private static String statement = "SELECT * FROM product";

    public static int insertProduct(Product product) {
        Connection dbConn = ConnectionFactory.getConnection();
        PreparedStatement insertS = null;

        int insertedID = -1;

        try
        {
            insertS = dbConn.prepareStatement(insertStatement, Statement.RETURN_GENERATED_KEYS);

            insertS.setString(1, product.getProductName());
            insertS.setInt(2, product.getProductPrice());
            insertS.executeUpdate();

            ResultSet rs = insertS.getGeneratedKeys();
            if (rs.next()) {
                insertedID = rs.getInt(1);
            }
        }
        catch(SQLException e)
        {
            System.out.println("SQLException: " + e.getMessage());
        }

        return insertedID;
    }

    public static void updateProduct(Product product) {
        Connection dbConn = ConnectionFactory.getConnection();
        PreparedStatement updateS = null;

        int updatedID = product.getProductID();

        try {
            updateS = dbConn.prepareStatement(updateStatement);

            updateS.setString(1, product.getProductName());
            updateS.setInt(2, product.getProductPrice());
            updateS.setInt(3,product.getProductID());
            updateS.executeUpdate();

        } catch (SQLException e) {
            System.out.println("SQLException: " + e.getMessage());


        }


    }

    public static void deleteProduct(Product product) {
        Connection dbConn = ConnectionFactory.getConnection();
        PreparedStatement deleteS = null;

        int deletedID = product.getProductID();

        try {
            deleteS = dbConn.prepareStatement(deleteStatement);
            deleteS.setInt(1,product.getProductID());
            deleteS.executeUpdate();

        } catch (SQLException e) {
            System.out.println("SQLException: " + e.getMessage());
        }

    }

    public static List<Product> showProducts(){
        Connection dbConn = ConnectionFactory.getConnection();
        PreparedStatement selectS = null;
        List<Product> products = new ArrayList<Product>();
        ResultSet resultSet;

        try {
            selectS = dbConn.prepareStatement(statement);
            resultSet = selectS.executeQuery();
            while(resultSet.next()){
                int ID = resultSet.getInt("productID");
                String name = resultSet.getString("productName");
                int price = resultSet.getInt("productPrice");
                Product product = new Product(ID,name,price);
                products.add(product);
            }
        } catch (SQLException e) {
            System.out.println("SQLException: " + e.getMessage());
        }

        return products;
    }
    
    
    
    
}
