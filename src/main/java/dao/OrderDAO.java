package dao;

import connection.ConnectionFactory;
import model.Order;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class OrderDAO {

    private static String insertStatement;

    static {
        insertStatement = "INSERT INTO `order` (productID, clientID, quantity)" + " VALUES (?,?,?)";
    }

    private static String updateStatement;
    private static String deleteStatement;

    static {
        updateStatement = "UPDATE `order` SET productID=?, clientID=?, quantity=? WHERE orderID=?";
        deleteStatement = "DELETE FROM `order` WHERE orderID=?";
    }

    private static String findStatement = "SELECT * FROM `order` WHERE orderID=?";
    private static String statement;

    static {
        statement = "SELECT * FROM `order`";
    }

    public static int insertOrder(Order order) {
        Connection dbConn = ConnectionFactory.getConnection();
        PreparedStatement insertS = null;

        int insertedID = -1;

        try
        {
            insertS = dbConn.prepareStatement(insertStatement, Statement.RETURN_GENERATED_KEYS);

            insertS.setInt(1, order.getProductID());
            insertS.setInt(2, order.getClientID());
            insertS.setInt(3, order.getQuantity());
            insertS.executeUpdate();

            ResultSet rs = insertS.getGeneratedKeys();
            if (rs.next()) {
                insertedID = rs.getInt(1);
            }
        }
        catch(SQLException e)
        {
            System.out.println("SQLException: " + e.getMessage());
        }

        return insertedID;
    }

    public static void updateOrder(Order order) {
        Connection dbConn = ConnectionFactory.getConnection();
        PreparedStatement updateS = null;

        int updatedID = order.getOrderID();

        try {
            updateS = dbConn.prepareStatement(updateStatement);

            updateS.setInt(1, order.getProductID());
            updateS.setInt(2, order.getClientID());
            updateS.setInt(3, order.getQuantity());
            updateS.setInt(4, order.getOrderID());

            updateS.executeUpdate();

        } catch (SQLException e) {
            System.out.println("SQLException: " + e.getMessage());


        }


    }

    public static void deleteOrder(Order order) {
        Connection dbConn = ConnectionFactory.getConnection();
        PreparedStatement deleteS = null;

        int deletedID = order.getOrderID();

        try {
            deleteS = dbConn.prepareStatement(deleteStatement);
            deleteS.setInt(1,order.getOrderID());
            deleteS.executeUpdate();

        } catch (SQLException e) {
            System.out.println("SQLException: " + e.getMessage());
        }

    }

    public static List<Order> showOrders(){
        Connection dbConn = ConnectionFactory.getConnection();
        PreparedStatement selectS = null;
        List<Order> orders = new ArrayList<Order>();
        ResultSet resultSet;

        try {
            selectS = dbConn.prepareStatement(statement);
            resultSet = selectS.executeQuery();
            while(resultSet.next()){
                int ID = resultSet.getInt("orderID");
                int productID = resultSet.getInt("productID");
                int clientID = resultSet.getInt("clientID");
                int quantity = resultSet.getInt("quantity");
                Order order = new Order(ID,productID,clientID,quantity);
                orders.add(order);
            }
        } catch (SQLException e) {
            System.out.println("SQLException: " + e.getMessage());
        }

        return orders;
    }
    
    
}
